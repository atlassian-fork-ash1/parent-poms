#!/usr/bin/env groovy

import groovy.io.FileType

def files = []
def expected_files = [
  "test-pom-${projectVersion}.pom.asc",
  "test-pom-${projectVersion}.jar.asc",
]

new File("${targetDir}/it/closedsource-pom-test/target/").eachFile(FileType.FILES) { file ->
  files << "${file.name}"
}

return ( expected_files.inject(true, { expected_files_present, expected_file ->
  expected_files_present && expected_file in files
}) )
